package net.openhft.chronicle.network;

/**
 * Created by andre on 07/10/2015.
 */
public enum SessionMode {
    ACTIVE,
    PASSIVE
}
